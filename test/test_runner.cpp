/*
 * test_runner.cpp
 *
 *  Created on: Jul 18, 2012
 *      Author: san-ss
 */

#include <gtest/gtest.h>

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
