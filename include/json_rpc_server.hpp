/*
 * Copyright (c) 2012, Nasel Security
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef JSON_RCP_SERVER_HPP_
#define JSON_RCP_SERVER_HPP_

#include <string>
#include <vector>
#include <stdint.h>

#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
//#include <boost/date_time/posix_time/posix_time.hpp>

#include "ssl_connection.hpp"
#include "json_rpc_parser.hpp"

namespace json_rpc {

typedef std::shared_ptr<boost::asio::ssl::context> context_ptr;

/**
 * \brief Its the server itself. Handles the SslConnections.
 *
 * This is the main class of the library. In order to start a server
 * an object of this type has to instantiated and call the run method.
 * To stop the server call its stop method.
 */
class JsonRpcServer : private boost::noncopyable {

public:

    /**
     * \brief Constructor for the JsonRpcServer
     *
     * Constructs a JsonRpcServer taking the listening address and port,
     * the number of threads to have in the pool and a [JsonRpcParser](@ref JsonRpcParser)
     *
     * @param address The IP address to listen on.
     * @param port The port to listen on.
     * @param context Shared pointer to the ssl_context to use.
     * @param thread_pool_size The number of threads to have in the thread pool.
     * @param rpc_parser [JsonRpcParser](@ref JsonRpcParser) to handle the RPC requests.
     */
    explicit JsonRpcServer(const std::string& address,
                              uint16_t port,
                              context_ptr context,
                              std::size_t thread_pool_size,
                              JsonRpcParser& rpc_parser);

    /**
     * \brief Start the server.
     */
    void run();

    /**
     * \brief Stop the server.
     */
    void stop();

private:

    /**
     * \brief Set up the next connection to be accepted.
     */
    void start_accept();

    /**
     * \brief Perform the handshake after connection is established.
     *
     * Method called asynchronously when a connection has been established,
     * in case of an error, error will not be NULL.
     *
     * @param error
     */
    void handle_accept(const boost::system::error_code& error);

    /**
     * \brief Handle the stopping of the server. Triggered by a signal.
     *
     * Method called asynchronously when a SIGINT, SIGTERM or SIGQUIT signal
     * is received.
     *
     */
    void handle_stop();

    /**
     * \brief The number of threads in the pool.
     */
    std::size_t thread_pool_size_;

    /**
     * \brief The io_service in charge of the async operations
     */
    boost::asio::io_service io_service_;

    /**
     * \brief The tcp acceptor who takes care of listening and accepting connections.
     */
    boost::asio::ip::tcp::acceptor acceptor_;

    /**
     * \brief Shared pointer to the next connection to be accepted.
     */
    ssl_connection_ptr new_connection_;

    /**
     * \brief SSL Context shared_ptr of the server.
     */
    context_ptr context_;

    /**
     * \brief [JsonRpcParser](@ref JsonRpcParser) holding the methods to accept.
     */
    JsonRpcParser& rpc_parser_;
//    request_handler request_handler_;

};

}

#endif /* JSON_RCP_SERVER_HPP_ */
