/*
 * Copyright (c) 2012, Nasel Security
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SSL_CONNECTION_HPP_
#define SSL_CONNECTION_HPP_

#include <cstdlib>
#include <iostream>


#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/array.hpp>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
//#include <boost/date_time/posix_time/posix_time.hpp>

#include "json_rpc_parser.hpp"

typedef boost::asio::ssl::stream<boost::asio::ip::tcp::socket> ssl_socket;

namespace json_rpc {

/**
 * \brief Represent the connection (over SSL), control the flow of the connection.
 *
 * This class is used to abstract the connection. It is in charge of controlling
 * what events have to happen in the communication flow (handshake, reception
 * of the RPC command and sending the response if needed).
 *
 */
class SslConnection : public boost::enable_shared_from_this<SslConnection>, private boost::noncopyable {

public:

    /**
     * \brief Construct an object receiving the io_service, context, JsonRpcParser
     * and the restriction parameters.
     *
     * @param io_service The io_service to use.
     * @param ctx The SSL context to be used for the connection.
     * @param msecs_per_transmission
     * @param msecs_per_connection
     * @param rpc_parser Reference to the [JsonRpcParser](@ref JsonRpcParser) to be used.
     */
    explicit SslConnection(boost::asio::io_service& io_service,
            boost::asio::ssl::context& ctx,
            uint64_t msecs_per_transmission,
            uint64_t msecs_per_connection,
            JsonRpcParser& rpc_parser);

    /**
     * \brief Return the low level tcp socket.
     *
     * @return The tcp socket over which this SSL connection has been established.
     */
    ssl_socket::lowest_layer_type& socket();

    /**
     * \brief Initiate the sequence of events.
     *
     * This method sets off the sequence of events that are expected for an
     * RPC connection.
     */
    void set_off();

private:

    /**
     * \brief Handler for the asynchronous handshake.
     *
     * This method is called asynchronously after the handshake has been done.
     * It sets up the waiting for the next event.
     *
     * @param ec If an error occurred during the handshake, this will indicate it.
     */
    void handle_handshake(const boost::system::error_code& ec);

    /**
     * \brief Handler for the asynchronous reading.
     *
     * This method is called asynchronously after reading a null byte.
     * It dispatches what has been read to the JsonRpcParser and if
     * needed sets up the asynchronous writing.
     *
     * @param ec If an error occurred during the handshake, this will indicate it.
     */
    void handle_read(const boost::system::error_code& ec);

    void handle_write(const boost::system::error_code& ec, size_t bytes_transferred);

    /**
     * \brief SSL stream of the connection.
     */
    ssl_socket  socket_;

    /**
     * \brief The strand being used.
     *
     * The strand is used to forbid concurrent calls to handlers of the same
     * connection.
     *
     */
    boost::asio::io_service::strand strand_;

    /**
     * \brief Buffer where data read is stored.
     */
    boost::asio::streambuf data_buffer_;

    boost::posix_time::time_duration msec_pt_;

    boost::asio::deadline_timer transmission_timer_;

    boost::posix_time::time_duration msec_pc_;

    boost::asio::deadline_timer connection_timer_;

    /**
     * \brief [JsonRpcParser](@ref JsonRpcParser) reference to take care
     * of the RPC itself.
     */
    JsonRpcParser& rpc_parser_;

};

typedef boost::shared_ptr<SslConnection> ssl_connection_ptr;

}


#endif /* SSL_CONNECTION_HPP_ */
